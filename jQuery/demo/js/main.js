// $("h1").greenify({
// 	color: "orange",
// 	backgroundColor: "transparent"
// });

// $.each([ 52, 97 ], function( index, value ) {
// 	alert( index + ": " + value );
// });

// var obj = {
// 	"flammable": "inflammable",
// 	"duh": "no duh"
// };
// $.each( obj, function( key, value ) {
// 	alert( key + ": " + value );
// });

// var arr = [ "one", "two", "three", "four", "five" ];
// var obj = { one: 1, two: 2, three: 3, four: 4, five: 5 };

// jQuery.each( arr, function( i, val ) {
// 	$( "#" + val ).text( "Mine is " + val + "." );

// 	// Will stop running after "three"
// 	return ( val !== "three" );
// });

// jQuery.each( obj, function( i, val ) {
// 	$( "#" + i ).append( document.createTextNode( " - " + val ) );

// 	// Will stop running after "three"
// 	return ( val !== 3 );
// });