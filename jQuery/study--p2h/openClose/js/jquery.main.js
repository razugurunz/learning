//openClose jQuery
jQuery(document).ready(function(){
	var elems = jQuery('.open-close');
	var activeClass = 'active';

	elems.each(function(){
		var self = jQuery(this);
		var opener = self.find('.opener');
		var slider = self.find('.slide');
		if(!self.hasClass(activeClass)){
			hideSlide();
		} 

		opener.click(function(e){
			e.preventDefault();
			if(self.hasClass(activeClass)){
				hideSlide();
			} else {
				self.siblings().removeClass(activeClass);
				showSlide();
			}
		});

		function showSlide() {
			slider.stop().slideDown();
			self.siblings().find('.slide').slideUp();
			self.addClass(activeClass);
		}

		function hideSlide() {
			slider.stop().slideUp();
			self.removeClass(activeClass);
		}
	});
});