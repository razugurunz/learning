#Basic syntax is: $(selector).action()

#The Document Ready Event:
	$(document).ready(function(){
		// jQuery methods go here...
	});
	// or //
	$(function(){
		// jQuery methods go here...
	});

	Note: This is to prevent any jQuery code from running before the document is finished loading.

# Every DOM node has its own set of proporties and methods. You may view all proporties of DOM node by using following console method - console.dir(objectToInspect) [[f.e. console.dir(document);]]

#Learn javascript basics here 1. https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/JavaScript_basics , 2. https://guide.freecodecamp.org/javascript

http://js-academy.w3.ua/?article=02_browserjs&section=styles-of-dom-nodes